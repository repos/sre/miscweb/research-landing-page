# Wikimedia Research Landing Page

View the page at https://research.wikimedia.org

To report an issue, please use [Phabricator](https://phabricator.wikimedia.org/maniphest/task/edit/form/1/?projects=research-landing-page).

## Development

## Blubber container image

Blubber images for research landing page.

Mirror of https://gerrit.wikimedia.org/g/research/landing-page/

### Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/research-landing-page:<timestamp>`

_Automated by [GitLab CI](https://gitlab.wikimedia.org/repos/sre/miscweb/research-landing-page/-/blob/master/.gitlab-ci.yml)._

## Deploy changes

- Merge changes to master branch.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-research-landing-page.yaml#3) with [latest value](https://gitlab.wikimedia.org/repos/sre/miscweb/research-landing-page/-/jobs/). _The value is in the `#13` step in the job 'publish-image'._

- Add wmf-sre-collab (group) as reviewer and wait for merge.

- Deploy to production from the deployment server. 

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).

## Content conventions

- On `.list--updates` keep a max of 9 items. Remove the oldest when adding a new item.
- Use [ACM](https://www.acm.org/publications/authors/reference-formatting) citation style.

## See also

- [Documentation on WikiOffice](https://office.wikimedia.org/wiki/Research/Landing_page)
- [Grafana Monitoring](https://grafana.wikimedia.org/d/exdTE7kSk/miscweb-k8s?orgId=1)
